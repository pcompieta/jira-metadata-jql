package com.paolocompieta.jira.plugin.metadatajql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.osoboo.jira.metadata.MetadataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class MetadataValueFunction extends AbstractJqlFunction {

    private static final Logger log = LoggerFactory.getLogger(MetadataValueFunction.class);

    private final ProjectManager projectManager;
    private final MetadataService metadataService;

    public MetadataValueFunction(ProjectManager projectManager, MetadataService metadataService) {
        this.projectManager = projectManager;
        this.metadataService = metadataService;
    }

    @Override
    public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause) {
        return validateNumberOfArgs(operand, getMinimumNumberOfExpectedArguments());
    }

    @Override
    public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause) {
        final List<QueryLiteral> literals = new LinkedList<QueryLiteral>();

        final List<String> arguments = operand.getArgs();
        final String fieldName = arguments.get(0);
        final String expectedFieldValue = arguments.get(1);
        log.info("Searching: field=" + fieldName + "; value=" + expectedFieldValue);

        if(metadataService==null){
            log.error("MetadataService is null.");
            return literals;
        }

        final List<Project> projectObjects = projectManager.getProjectObjects();
        for (Project project : projectObjects) {
            final String metadataValue = metadataService.getMetadataValue(project, fieldName);
            if(metadataValue==null){
                log.info("MetadataValueFunction: project=" + project.getKey() + " has no " + fieldName + " defined.");
                continue;
            }
            log.info("MetadataValueFunction: project=" + project.getKey() + "; value=" + metadataValue);
            if(expectedFieldValue.equals(metadataValue)) {
                literals.add(new QueryLiteral(operand, project.getKey()));
            }
        }
        return literals;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 2;
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.PROJECT;
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public String getFunctionName() {
        return "projectMetadataValue";
    }
}
